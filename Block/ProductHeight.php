<?php

namespace Encomage\ProductAdditionalAttributes\Block;

use \Magento\Catalog\Block\Product\View;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Framework\Url\EncoderInterface;
use \Magento\Framework\Json\EncoderInterface as JsonEncoderInterface;
use \Magento\Framework\Stdlib\StringUtils;
use \Magento\Catalog\Helper\Product;
use \Magento\Catalog\Model\ProductTypes\ConfigInterface;
use \Magento\Framework\Locale\FormatInterface;
use \Magento\Customer\Model\Session;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Framework\Pricing\PriceCurrencyInterface;

class ProductHeight extends View
{
    /**
     * @param Context $context
     * @param EncoderInterface $urlEncoder
     * @param JsonEncoderInterface $jsonEncoder
     * @param StringUtils $string
     * @param Product $productHelper
     * @param ConfigInterface $productTypeConfig
     * @param FormatInterface $localeFormat
     * @param Session $customerSession
     * @param ProductRepositoryInterface $productRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(Context                    $context,
                                EncoderInterface           $urlEncoder,
                                JsonEncoderInterface       $jsonEncoder,
                                StringUtils                $string,
                                Product                    $productHelper,
                                ConfigInterface            $productTypeConfig,
                                FormatInterface            $localeFormat,
                                Session                    $customerSession,
                                ProductRepositoryInterface $productRepository,
                                PriceCurrencyInterface     $priceCurrency,
                                array                      $data = [])
    {
        parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $data);
    }

    /**
     * @return string
     */
    public function getProductHeight()
    {
        $currentProduct = $this->getProduct();
        $productHeight = $currentProduct->getCustomAttribute('product_height');
        if ($productHeight !== null) {
            return $productHeight->getValue();
        }
    }

    /**
     * @return bool
     */
    public function isShowProductHeight()
    {
        $currentProduct = $this->getProduct();
        $showingProductHeight = $currentProduct->getCustomAttribute('showing_product_height');

        //If frontend display is enabled
        if ($showingProductHeight !== null && $showingProductHeight->getValue() == 1) {
            return true;
        }
        return false;
    }
}
